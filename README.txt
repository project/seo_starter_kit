SEO Starter Kit
==================

SEO is one of the most important and difficult parts of the work performed on the site. But despite its complexity SEO solves the same problems from site to site.

Drupal provides a lot of tools aimed at SEO. And after you find them you will have a few question:

how to choose the most appropriate tools from them
how to correctly configure these tools to achieve maximum improvement SEO for this site
how to refuse of manual configuration and apply automation.
SEO Profile was created to save your time and to help with following problems:

1. Search Engine Friendly

    Robots txt
    Google analytics
    Redirect
    Metatag
    
2. User Friendly

    Sitemap
    Path Breadcrumbs
    External Links
    Search 404

3. Performance

    Advanced CSS/JS Aggregation
    Fences
    Remove default css files
    
SEO is constantly changing and evolving and this profile also should not stand still, so if you have suggestions or comments, please report it to us.


INSTALLATION
------------

To install the profile:

- Unzip the folder and copy it to necessary folder of your server.
- Install your new website with the SEO Starter Kit. It’s set by default, you just need to click the “Save and continue” button and follow the instructions.
- Wait for the SEO Starter Kit to be installed, enable the necessary modules and enjoy your website.

After install you need to:

1. Remove robots txt file
2. Configure the "Google Analytics" module
3. Put the "Sitemap" block to the bottom of your site


MAINTAINERS
-----------

ADCI solutions team
